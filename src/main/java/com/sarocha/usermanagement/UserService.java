/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sarocha.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author Sarocha
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    //Mockup
    static {
        userList.add(new User("admin", "password"));
    }

    //create
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }

    //update
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    //read 1 user
    public static User getUser(int index) {
        if (index > userList.size() - 1) {
            return null;
        }
        return userList.get(index);
    }

    public static ArrayList<User> getUsers() {
        return userList;
    }

    //search username
    public static ArrayList<User> searchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return list;
    }

    //delete user
    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    // Login
    public static User login(String userName, String password) {
        for (User user : userList) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }
    
    public static void save() {
        
    }
    
    public static void load() {
        
    }
}
